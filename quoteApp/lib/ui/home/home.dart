import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/vector.jpg'), fit: BoxFit.cover)),
      height: MediaQuery.of(context).size.height,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 70),
        child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text:
                    "If you look at what you have in life, you'll always have more. If you look at what you don't have in life, you'll never have enough. \n\n",
                style: TextStyle(color: Colors.black, fontSize: 27),
                children: [
                  TextSpan(
                    text: 'Oprah Winfrey',
                    style: TextStyle(color: Colors.black, fontSize: 27),
                  )
                ])),
      ),
    ));
  }
}
