import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:quoteApp/ui/widget/drawer.widget.dart';

class DrawerApp extends StatefulWidget {
  @override
  _DrawerAppState createState() => _DrawerAppState();
}

class _DrawerAppState extends State<DrawerApp> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          createHeader(context),
          createDrawerItem(
            icon: EvaIcons.settings2,
            text: 'Settings',
          ),
          createDrawerItem(
            icon: EvaIcons.share,
            text: 'Invite a Friend',
          ),
          Divider(thickness: 2, color: Colors.grey),
          createDrawerItem(icon: EvaIcons.person, text: 'About Us'),
          createDrawerItem(icon: EvaIcons.email, text: 'Contact Us'),
          createDrawerItem(icon: EvaIcons.star, text: 'Rate Us'),
          createDrawerItem(icon: EvaIcons.fileText, text: 'Feedback'),
          Divider(thickness: 2, color: Colors.grey),
          createDrawerItem(icon: EvaIcons.shield, text: 'Privacy Policy'),
          createDrawerItem(icon: EvaIcons.layers, text: 'Terms of Service')
        ],
      ),
    );
  }
}
