import 'package:flutter/material.dart';
import 'package:quoteApp/model/user.model.dart';
import 'package:quoteApp/ui/quote/quotes.dart';

class QuoteCategory extends StatefulWidget {
  @override
  _QuoteCategoryState createState() => _QuoteCategoryState();
}

class _QuoteCategoryState extends State<QuoteCategory> {
  List _detailTypeList = List<Details>();

  @override
  void initState() {
    super.initState();
    _detailTypeList =
        Details('tharindu', 'desc', 'life', Colors.cyan).createSampleList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
          child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Expanded(
              child: GridView.builder(
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                  ),
                  itemCount: _detailTypeList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Quotes()),
                        );
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Colors.greenAccent),
                          child: Row(
                            children: [
                              SizedBox(
                                width: 15,
                              ),
                              Flexible(
                                  child: RichText(
                                text: TextSpan(
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                    fontWeight: FontWeight.normal,
                                  ),
                                  text: _detailTypeList[index].category,
                                ),
                              ))
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            )
          ],
        ),
      )),
    );
  }
}
