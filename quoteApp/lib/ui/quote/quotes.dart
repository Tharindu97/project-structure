import 'package:flutter/material.dart';
import 'package:quoteApp/model/user.model.dart';


class Quotes extends StatefulWidget {

  @override
  _QuotesState createState() => _QuotesState();
}

class _QuotesState extends State<Quotes> {

  List _detailTypeList = List<Details>();
  double _screenWidthAdjustment;
  

  @override
  void initState() {
    super.initState();
    _detailTypeList = Details('tharindu','desc', 'life', Colors.cyan).createSampleList();
  }

@override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _screenWidthAdjustment = MediaQuery.of(context).size.width - 48.0 - 64.0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemExtent: 150.0,
                  itemCount: _detailTypeList.length,
                  itemBuilder: (BuildContext context, int index){
                    return GestureDetector(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          height: 120,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.blueAccent,
                          ),
                          child: Stack(
                            children: [
                              Positioned(
                                top: 20.0,
                                left: 20.0,
                                width: _screenWidthAdjustment,
                                child: Material(
                                  color: Colors.transparent,
                                  child: Text(
                                    _detailTypeList[index].desc,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold),
                                    maxLines: 3,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                ),
              )
            ],
          ),
        )
      ),
    );
  }
}


