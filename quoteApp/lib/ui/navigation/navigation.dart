import 'package:flutter/material.dart';
import 'package:quoteApp/ui/author/author.dart';
import 'package:quoteApp/ui/drawer/drawer.dart';
import 'package:quoteApp/ui/home/home.dart';
import 'package:quoteApp/ui/quote.category/quote.category.dart';
import 'package:quoteApp/ui/quote/quotes.dart';
import 'package:quoteApp/ui/widget/navigator.dart';


class NavigationBar extends StatefulWidget {
  @override
  _NavigationBarState createState() => _NavigationBarState();
}

class _NavigationBarState extends State<NavigationBar> {
  
  int index = 2;
  List<Widget> widgetOption = <Widget>[
    DrawerApp(),
    Quotes(),
    Home(),
    Author(),
    QuoteCategory()
  ];

  void changeIndexNumber(value) {
    setState(() {
      index = value;
    });
  }

  @override
  
  Widget build(BuildContext context) {
   return Scaffold(
      body: widgetOption.elementAt(index),
      bottomNavigationBar: navigationBar(index: index, action: changeIndexNumber),
    );
  }
}