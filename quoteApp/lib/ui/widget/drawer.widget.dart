import 'package:flutter/material.dart';
Widget createHeader(context) {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill, image: AssetImage('assets/header.png')
          )
        ),
        child: Stack(children: <Widget>[
          Positioned(
            left: 20,
            bottom: 12,
            child: Text('Settings',
              style: TextStyle(
                fontSize: 30,
                fontWeight: FontWeight.bold
              ),
            )
          )
        ]));
  }

  Widget createDrawerItem({IconData icon, String text, GestureTapCallback onTap}) {
    return ListTile(
      title: Row(
        children: <Widget>[
          Icon(
            icon,
            color: Colors.grey,
          ),
          Padding(
            padding: EdgeInsets.only(left: 8.0),
            child: Text(
              text,
              style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
      onTap: onTap,
    );
  }