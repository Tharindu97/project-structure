import 'package:flutter/material.dart';
import 'package:quoteApp/ui/navigation/navigation.dart';


void main() {
  runApp(
     MaterialApp(
      debugShowCheckedModeBanner: false,
      home: NavigationBar(),
    )
  );
}

