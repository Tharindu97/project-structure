import 'package:flutter/material.dart';

class Details{
  String title;
  String category;
  String desc;
  MaterialColor materialColor;

  Details(
    this.title,
    this.category,
    this.desc,
    this.materialColor
  );

  List<Details> createSampleList(){
    List _detailsType = List<Details>();
    return _detailsType
      ..add(Details(
        'Nelson Mandela',
        'Life',
        'Keep these quotes bookmarked on your phone or computer to pull up and scroll through whenever you need a little pick me up.',
         Colors.blue
        ))
      ..add(Details(
        'Nelson Mandela',
        'Life',
        'Keep these quotes bookmarked on your phone or computer to pull up and scroll through whenever you need a little pick me up.', 
         Colors.blue
        ))
      ..add(Details(
        'Nelson Mandela',
        'Life', 
        'Keep these quotes bookmarked on your phone or computer to pull up and scroll through whenever you need a little pick me up.', 
         Colors.blue
        ))
      ..add(Details(
        'Nelson Mandela',
        'Life', 
        'Keep these quotes bookmarked on your phone or computer to pull up and scroll through whenever you need a little pick me up.', 
         Colors.blue
        ))
      ..add(Details(
        'Nelson Mandela',
        'Life', 
        'Keep these quotes bookmarked on your phone or computer to pull up and scroll through whenever you need a little pick me up.', 
         Colors.blue
        ))
      ..add(Details(
        'Nelson Mandela',
        'Life', 
        'Keep these quotes bookmarked on your phone or computer to pull up and scroll through whenever you need a little pick me up.', 
         Colors.blue
        ))
      ..add(Details(
        'Nelson Mandela',
        'Life', 
        'Keep these quotes bookmarked on your phone or computer to pull up and scroll through whenever you need a little pick me up.', 
         Colors.blue
        ))
      ..add(Details(
        'Nelson Mandela',
        'Life', 
        'Keep these quotes bookmarked on your phone or computer to pull up and scroll through whenever you need a little pick me up.', 
         Colors.blue
        ))
      ..add(Details(
        'Nelson Mandela',
        'Life', 
        'Keep these quotes bookmarked on your phone or computer to pull up and scroll through whenever you need a little pick me up.', 
         Colors.blue
        ));
  }
}